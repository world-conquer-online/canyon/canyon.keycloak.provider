package org.world.conquer.canyon.keycloak.provider;

import lombok.extern.jbosslog.JBossLog;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.models.*;
import org.keycloak.models.cache.CachedUserModel;
import org.keycloak.models.credential.PasswordCredentialModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.user.UserLookupProvider;
import org.keycloak.storage.user.UserQueryProvider;
import org.world.conquer.canyon.keycloak.provider.repository.UserRepository;

import java.util.Map;
import java.util.stream.Stream;

@JBossLog
public class CanyonUserStorageProvider implements UserStorageProvider,
        UserLookupProvider,
        CredentialInputValidator,
        UserQueryProvider {

    private final MySQLProvider mySQLProvider;
    private final UserRepository userRepository;

    public CanyonUserStorageProvider(KeycloakSession keycloakSession,
                                     ComponentModel componentModel,
                                     MySQLProvider mySQLProvider) {
        this.mySQLProvider = mySQLProvider;
        userRepository = new UserRepository(mySQLProvider, keycloakSession, componentModel);
    }

    @Override
    public boolean supportsCredentialType(String credentialType) {
        return PasswordCredentialModel.TYPE.equals(credentialType);
    }

    @Override
    public boolean isConfiguredFor(RealmModel realmModel, UserModel userModel, String credentialType) {
        return supportsCredentialType(credentialType);
    }

    @Override
    public boolean isValid(RealmModel realmModel, UserModel userModel, CredentialInput credentialInput) {
        log.infov("isValid user credential: userId={0}", userModel.getId());
        if (!supportsCredentialType(credentialInput.getType()) || !(credentialInput instanceof UserCredentialModel cred)) {
            return false;
        }

        UserModel dbUser = userModel;
        // If the cache just got loaded in the last 500 millisec (i.e. probably part of the actual flow), there is no point in reloading the user.)
        if (userModel instanceof CachedUserModel && (System.currentTimeMillis() - ((CachedUserModel) userModel).getCacheTimestamp()) > 500) {
            dbUser = this.getUserById(realmModel, userModel.getId());

            if (dbUser == null) {
                ((CachedUserModel) userModel).invalidate();
                return false;
            }

            // For now, we'll just invalidate the cache if username or email has changed. Eventually we could check all (or a parametered list of) attributes fetched from the DB.
            if (!java.util.Objects.equals(userModel.getUsername(), dbUser.getUsername()) || !java.util.Objects.equals(userModel.getEmail(), dbUser.getEmail())) {
                ((CachedUserModel) userModel).invalidate();
            }
        }
        return userRepository.validateCredentials(dbUser.getUsername(), cred.getChallengeResponse());
    }

    @Override
    public void close() {
        log.info("close");
        mySQLProvider.close();
    }

    @Override
    public UserModel getUserById(RealmModel realmModel, String id) {
        log.infov("lookup user by id: realm={0} userId={1}", realmModel.getId(), id);
        final String externalId = StorageId.externalId(id);
        return this.userRepository.getById(realmModel, externalId);
    }

    @Override
    public UserModel getUserByUsername(RealmModel realmModel, String userName) {
        log.infov("lookup user by username: realm={0} username={1}", realmModel.getId(), userName);
        return this.userRepository.getByUsername(realmModel, userName);
    }

    @Override
    public UserModel getUserByEmail(RealmModel realmModel, String email) {
        log.infov("lookup user by email: realm={0} email={1}", realmModel.getId(), email);
        return this.userRepository.getByEmail(realmModel, email);
    }

    @Override
    public Stream<UserModel> searchForUserStream(RealmModel realmModel, String search, Integer firstResult, Integer maxResults) {
        log.infov("search for users with params: realm={0} search={1} firstResult={2} maxResults={3}", realmModel.getId(), search, firstResult, maxResults);
        return this.userRepository.getByTerm(realmModel, search).stream();
    }

    @Override
    public Stream<UserModel> searchForUserStream(RealmModel realmModel, Map<String, String> map, Integer firstResult, Integer maxResults) {
        log.infov("search for users with params: realm={0} params={1} firstResult={2} maxResults={3}", realmModel.getId(), map, firstResult, maxResults);
        return this.searchForUserStream(realmModel, map.values().stream().findFirst().orElse(""));
    }

    @Override
    public Stream<UserModel> getGroupMembersStream(RealmModel realmModel, GroupModel groupModel, Integer firstResult, Integer maxResults) {
        log.infov("search for group members: realm={0} groupId={1} firstResult={2} maxResults={3}", realmModel.getId(), groupModel.getId(), firstResult, maxResults);
        return Stream.empty();
    }

    @Override
    public Stream<UserModel> searchForUserByUserAttributeStream(RealmModel realmModel, String attrName, String attrValue) {
        log.infov("search for group members: realm={0} attrName={1} attrValue={2}", realmModel.getId(), attrName, attrValue);
        return Stream.empty();
    }

    @Override
    public int getUsersCount(RealmModel realm, boolean includeServiceAccount) {
        return 0;
    }
}
