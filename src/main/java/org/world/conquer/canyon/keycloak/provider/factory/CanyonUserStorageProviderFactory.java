package org.world.conquer.canyon.keycloak.provider.factory;

import lombok.extern.jbosslog.JBossLog;
import org.keycloak.component.ComponentModel;
import org.keycloak.component.ComponentValidationException;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;
import org.keycloak.storage.UserStorageProviderFactory;
import org.world.conquer.canyon.keycloak.provider.CanyonUserStorageProvider;
import org.world.conquer.canyon.keycloak.provider.MySQLProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JBossLog
public class CanyonUserStorageProviderFactory
        implements UserStorageProviderFactory<CanyonUserStorageProvider> {

    private final Map<String, ProviderConfig> providerConfigPerInstance = new HashMap<>();

    @Override
    public String getId() {
        return "canyon-user-provider";
    }

    private synchronized ProviderConfig configure(ComponentModel model) {
        log.infov("Creating configuration for model: id={0} name={1}", model.getId(), model.getName());
        ProviderConfig providerConfig = new ProviderConfig();
        String user = model.get("user");
        String password = model.get("password");
        String url = model.get("url");
        providerConfig.dataSourceProvider.configure(url, user, password, model.getName());
        return providerConfig;
    }

    @Override
    public CanyonUserStorageProvider create(KeycloakSession session, ComponentModel model) {
        ProviderConfig providerConfig = providerConfigPerInstance.computeIfAbsent(model.getId(), s -> configure(model));
        return new CanyonUserStorageProvider(session, model, providerConfig.dataSourceProvider);
    }

    @Override
    public void close() {
        for (Map.Entry<String, ProviderConfig> pc : providerConfigPerInstance.entrySet()) {
            pc.getValue().dataSourceProvider.close();
        }
    }

    @Override
    public void validateConfiguration(KeycloakSession session, RealmModel realm, ComponentModel model) throws ComponentValidationException {
        try {
            ProviderConfig old = providerConfigPerInstance.put(model.getId(), configure(model));
            if (old != null) {
                old.dataSourceProvider.close();
            }
        } catch (Exception e) {
            throw new ComponentValidationException(e.getMessage(), e);
        }
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return ProviderConfigurationBuilder.create()
                //DATABASE
                .property()
                .name("url")
                .label("JDBC URL")
                .helpText("JDBC Connection String")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("jdbc:mysql://server-name/database_name")
                .add()
                .property()
                .name("user")
                .label("JDBC Connection User")
                .helpText("JDBC Connection User")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("user")
                .add()
                .property()
                .name("password")
                .label("JDBC Connection Password")
                .helpText("JDBC Connection Password")
                .type(ProviderConfigProperty.PASSWORD)
                .defaultValue("password")
                .add()
                .build();
    }

    private static class ProviderConfig {
        private final MySQLProvider dataSourceProvider = new MySQLProvider();
    }
}
