package org.world.conquer.canyon.keycloak.provider.repository;

import lombok.extern.jbosslog.JBossLog;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.world.conquer.canyon.keycloak.provider.MySQLProvider;
import org.world.conquer.canyon.keycloak.provider.model.ApplicationUserModel;
import org.world.conquer.canyon.keycloak.provider.password.CanyonWhirlpoolHash;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@JBossLog
public class UserRepository {
    private final MySQLProvider mysql;
    private final KeycloakSession keycloakSession;
    private final ComponentModel componentModel;

    public UserRepository(MySQLProvider mysql, KeycloakSession keycloakSession, ComponentModel componentModel) {
        this.mysql = mysql;
        this.keycloakSession = keycloakSession;
        this.componentModel = componentModel;
    }

    public ApplicationUserModel getByUsername(RealmModel realmModel, String userName) {
        return this.queryToUserModel(realmModel, "SELECT au.Id, au.UserName, au.Email, au.EmailConfirmed, " +
                " UNIX_TIMESTAMP(au.CreationDate) `CreationDate`, fn.ClaimValue `FirstName`, ln.ClaimValue `LastName` " +
                " FROM `account_user` au " +
                " LEFT JOIN `account_user_claims` fn ON fn.UserId = au.Id AND fn.ClaimType = 'given_name' " +
                " LEFT JOIN `account_user_claims` ln ON ln.UserId = au.Id AND ln.ClaimType = 'family_name' " +
                " WHERE au.`NormalizedUserName` = ? LIMIT 1;", userName.toUpperCase(Locale.ROOT));
    }

    public ApplicationUserModel getByEmail(RealmModel realmModel, String email) {
        return this.queryToUserModel(realmModel, "SELECT au.Id, au.UserName, au.Email, au.EmailConfirmed, " +
                " UNIX_TIMESTAMP(au.CreationDate) `CreationDate`, fn.ClaimValue `FirstName`, ln.ClaimValue `LastName` " +
                " FROM `account_user` au " +
                " LEFT JOIN `account_user_claims` fn ON fn.UserId = au.Id AND fn.ClaimType = 'given_name' " +
                " LEFT JOIN `account_user_claims` ln ON ln.UserId = au.Id AND ln.ClaimType = 'family_name' " +
                " WHERE au.`NormalizedEmail` = ? LIMIT 1;", email.toUpperCase(Locale.ROOT));
    }

    public ApplicationUserModel getById(RealmModel realmModel, String id) {
        return this.queryToUserModel(realmModel, "SELECT au.Id, au.UserName, au.Email, au.EmailConfirmed, " +
                " UNIX_TIMESTAMP(au.CreationDate) `CreationDate`, fn.ClaimValue `FirstName`, ln.ClaimValue `LastName` " +
                " FROM `account_user` au " +
                " LEFT JOIN `account_user_claims` fn ON fn.UserId = au.Id AND fn.ClaimType = 'given_name' " +
                " LEFT JOIN `account_user_claims` ln ON ln.UserId = au.Id AND ln.ClaimType = 'family_name' " +
                " WHERE au.`Id` = ? LIMIT 1;", id.toLowerCase(Locale.ROOT));
    }

    public boolean validateCredentials(String userName, String password) {
        String fullString = this.getString("SELECT CONCAT(PasswordHash, '$', Salt) FROM account_user WHERE NormalizedUserName = ? LIMIT 1;", userName.toUpperCase(Locale.ROOT));
        String[] information = fullString.split("\\$");
        if (information.length != 2) {
            log.warnf("User [{0}] invalid password format", userName);
            return false;
        }
        String hashedPassword = CanyonWhirlpoolHash.hashPassword(password, information[1], 0);
        return hashedPassword.equals(information[0]);
    }

    public List<UserModel> getByTerm(RealmModel realmModel, String search) {
        return this.getByTerm(realmModel, search, 0, 10);
    }

    public List<UserModel> getByTerm(RealmModel realmModel, String search, Integer firstResult, Integer maxResults) {
        final String normalizedSearch = String.format("%%%s%%", search).toUpperCase();
        if ("*".trim().equals(search) || "".trim().equals(search)) {
            return this.queryToUserList(realmModel, "SELECT au.Id, au.UserName, au.Email, au.EmailConfirmed, " +
                    " UNIX_TIMESTAMP(au.CreationDate) `CreationDate`, fn.ClaimValue `FirstName`, ln.ClaimValue `LastName` " +
                    " FROM `account_user` au " +
                    " LEFT JOIN `account_user_claims` fn ON fn.UserId = au.Id AND fn.ClaimType = 'given_name' " +
                    " LEFT JOIN `account_user_claims` ln ON ln.UserId = au.Id AND ln.ClaimType = 'family_name' " + limitQuery(firstResult, maxResults));
        }
        return this.queryToUserList(realmModel, "SELECT au.Id, au.UserName, au.Email, au.EmailConfirmed, " +
                " UNIX_TIMESTAMP(au.CreationDate) `CreationDate`, fn.ClaimValue `FirstName`, ln.ClaimValue `LastName` " +
                " FROM `account_user` au " +
                " LEFT JOIN `account_user_claims` fn ON fn.UserId = au.Id AND fn.ClaimType = 'given_name' " +
                " LEFT JOIN `account_user_claims` ln ON ln.UserId = au.Id AND ln.ClaimType = 'family_name' " +
                " WHERE au.`NormalizedEmail` LIKE ? OR au.`NormalizedUserName` LIKE ? " + limitQuery(firstResult, maxResults), normalizedSearch, normalizedSearch);
    }

    private final String limitQuery(Integer from, Integer limit) {
        return String.format(" LIMIT %d, %d ", from, limit);
    }

    private String getString(String query, Object... params) {
        if (this.mysql.getDataSource().isPresent()) {
            DataSource dataSource = this.mysql.getDataSource().get();
            try (Connection connection = dataSource.getConnection()) {
                try (PreparedStatement ps = connection.prepareStatement(query)) {
                    if (params != null) {
                        for (int i = 1; i <= params.length; i++) {
                            ps.setObject(i, params[i - 1]);
                        }
                    }
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return rs.getString(1);
                        }
                        return "";
                    }
                }
            } catch (SQLException e) {
                log.error(e.getMessage(), e);
            }
        }
        return "";
    }

    private ApplicationUserModel queryToUserModel(RealmModel realmModel, String query, Object... params) {
        if (this.mysql.getDataSource().isPresent()) {
            DataSource dataSource = this.mysql.getDataSource().get();
            try (Connection connection = dataSource.getConnection()) {
                try (PreparedStatement ps = connection.prepareStatement(query)) {
                    if (params != null) {
                        for (int i = 1; i <= params.length; i++) {
                            ps.setObject(i, params[i - 1]);
                        }
                    }

                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            return new ApplicationUserModel(keycloakSession, realmModel, componentModel,
                                    rs.getString("Id"),
                                    rs.getString("UserName"),
                                    rs.getString("Email"),
                                    rs.getString("FirstName"),
                                    rs.getString("LastName"),
                                    rs.getLong("CreationDate"),
                                    rs.getBoolean("EmailConfirmed"));
                        }
                        return null;
                    }
                }
            } catch (SQLException e) {
                log.error(e.getMessage(), e);
            }
        }
        return null;
    }

    private List<UserModel> queryToUserList(RealmModel realmModel, String query, Object... params) {
        if (this.mysql.getDataSource().isPresent()) {
            DataSource dataSource = this.mysql.getDataSource().get();
            try (Connection connection = dataSource.getConnection()) {
                try (PreparedStatement ps = connection.prepareStatement(query)) {
                    if (params != null) {
                        for (int i = 1; i <= params.length; i++) {
                            ps.setObject(i, params[i - 1]);
                        }
                    }
                    try (ResultSet rs = ps.executeQuery()) {
                        List<UserModel> result = new ArrayList<>();
                        while (rs.next()) {
                            result.add(new ApplicationUserModel(keycloakSession, realmModel, componentModel,
                                    rs.getString("Id"),
                                    rs.getString("UserName"),
                                    rs.getString("Email"),
                                    rs.getString("FirstName"),
                                    rs.getString("LastName"),
                                    rs.getLong("CreationDate"),
                                    rs.getBoolean("EmailConfirmed")));
                        }
                        return result;
                    }
                    catch (Exception e) {
                        log.errorf(e , "error when executing query: {0}", e.getMessage());
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return List.of();
    }
}
