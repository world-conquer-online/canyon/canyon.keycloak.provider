package org.world.conquer.canyon.keycloak.provider.password;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CanyonWhirlpoolHash {
    public static String whirlpoolHashOnce(String message) {
        if (message == null || message.trim().isEmpty()) {
            return "";
        }

        try {
            MessageDigest whirlpool = MessageDigest.getInstance("Whirlpool");

            byte[] data = message.getBytes(StandardCharsets.UTF_8);
            whirlpool.reset();
            whirlpool.update(data);

            byte[] ret = whirlpool.digest();
            return bytesToString(ret);
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static String hashPassword(String password, String salt, int version) {
        if (password == null || password.trim().isEmpty()) {
            return "";
        }

        String result = "";
        for (int i = 0; i < 15000; i++) {
            result = whirlpoolHashOnce(salt + password + salt);
        }

        result = String.format("%02d%s", version, result);
        return result;
    }

    private static String bytesToString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
}
