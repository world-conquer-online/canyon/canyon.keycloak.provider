package org.world.conquer.canyon.keycloak.provider.model;

import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.adapter.AbstractUserAdapterFederatedStorage;

public class ApplicationUserModel extends AbstractUserAdapterFederatedStorage {

    private final String id;
    private String userName;
    private String email;
    private String firstName;
    private String lastName;
    private Long createdTimestamp;
    private boolean emailConfirmed;

    public ApplicationUserModel(KeycloakSession session,
                                RealmModel realm,
                                ComponentModel storageProviderModel,
                                String id,
                                String userName,
                                String email,
                                String firstName,
                                String lastName,
                                Long createdTimestamp,
                                boolean emailConfirmed) {
        super(session, realm, storageProviderModel);
        this.id = StorageId.keycloakId(storageProviderModel, id);
        this.userName = userName;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.createdTimestamp = createdTimestamp;
        this.emailConfirmed = emailConfirmed;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public void setUsername(String s) {

    }

    @Override
    public Long getCreatedTimestamp() {
        return this.createdTimestamp;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getFirstName() {
        return this.firstName;
    }

    @Override
    public String getLastName() {
        return this.lastName;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public boolean isEmailVerified() {
        return this.emailConfirmed;
    }
}
